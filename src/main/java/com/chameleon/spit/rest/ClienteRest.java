package com.chameleon.spit.rest;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.chameleon.spit.dao.ClienteDao;
import com.chameleon.spit.dao.PessoaDao;
import com.chameleon.spit.model.Cliente;

@RestController
@RequestMapping("rest/cliente")
public class ClienteRest {

	@Autowired
	private ClienteDao clienteDao;

	@Autowired
	private PessoaDao pessoaDao;

	@GetMapping
	public String teste() {
		String teste = "teste";
		return teste;
	}

	@PostMapping("/save")
	public void save(@RequestBody Cliente cliente) {
		pessoaDao.save(cliente.getPessoa());
		clienteDao.save(cliente);
	}

	@GetMapping("/listAll")
	public List<Cliente> listAll() {
		try {
			return clienteDao.findAll();
		} catch (Exception e) {
			e.getCause();
		}
		return null;
	}

	@GetMapping("/delete/{id}")
	public void delete(@PathVariable Long id) {
		System.out.println(id);
		try {
			Optional<Cliente> clienteOpt = clienteDao.findById(id);	
			if(clienteOpt.isPresent()) {
				clienteDao.deleteById(id);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@GetMapping("/find-by-id/{id}")
	public Optional<Cliente> findById(@PathVariable Long id) {
		try {
			Optional<Cliente> clienteOpt = clienteDao.findById(id);
			return clienteOpt;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
