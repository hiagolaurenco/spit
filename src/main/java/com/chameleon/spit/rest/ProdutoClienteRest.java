package com.chameleon.spit.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.chameleon.spit.dao.ProdutoClienteDao;
import com.chameleon.spit.model.ProdutoCliente;

@RestController
@RequestMapping("rest/produtoCliente")
public class ProdutoClienteRest {
	
	@Autowired
	private ProdutoClienteDao produtoClienteDao;
	
	@PostMapping("/save")
	public void save(@RequestBody ProdutoCliente produtoCliente) {
		produtoClienteDao.save(produtoCliente);
	}
	
	@GetMapping("/listAll")
	public List<ProdutoCliente> listAll() {
		try {
			return produtoClienteDao.findAll();
		} catch (Exception e) {
			e.getCause();
		}
		return null;
	}
	
	@GetMapping("/listProdutoCliente/{clienteId}")
	public List<ProdutoCliente> listProdutoCliente(@PathVariable Long clienteId){
		System.out.println(clienteId);
		return produtoClienteDao.listProdutoClinte(clienteId);
		
	}

}
