package com.chameleon.spit.rest;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.chameleon.spit.dao.ProdutoDao;
import com.chameleon.spit.model.Produto;

@RestController
@RequestMapping("rest/produto")
public class ProdutoRest {

	@Autowired
	private ProdutoDao produtoDao;

	@GetMapping
	public String teste() {
		String teste = "teste";
		return teste;
	}

	@PostMapping("/save")
	public void save(@RequestBody Produto produto) {
		produtoDao.save(produto);
	}

	@GetMapping("/listAll")
	public List<Produto> listAll() {
		try {
			return produtoDao.findAll();
		} catch (Exception e) {
			e.getCause();
		}
		return null;
	}

	@GetMapping("/delete/{id}")
	public void delete(@PathVariable Long id) {
		System.out.println(id);
		try {
			Optional<Produto> clienteOpt = produtoDao.findById(id);	
			if(clienteOpt.isPresent()) {
				produtoDao.deleteById(id);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@GetMapping("/find-by-id/{id}")
	public Optional<Produto> findById(@PathVariable Long id) {
		try {
			Optional<Produto> clienteOpt = produtoDao.findById(id);
			return clienteOpt;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
