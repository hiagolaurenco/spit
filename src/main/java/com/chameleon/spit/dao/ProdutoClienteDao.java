package com.chameleon.spit.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.chameleon.spit.model.ProdutoCliente;

@Repository
public interface ProdutoClienteDao extends JpaRepository<ProdutoCliente, Long> {

	@Query("select pc from ProdutoCliente as pc where pc.cliente.id = :clienteId")
	List<ProdutoCliente> listProdutoClinte(@Param("clienteId") Long clienteId);
}
