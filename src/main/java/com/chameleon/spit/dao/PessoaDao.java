package com.chameleon.spit.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.chameleon.spit.model.Pessoa;

@Repository
public interface PessoaDao extends JpaRepository<Pessoa, Long>, CrudRepository<Pessoa, Long> {

}
