package com.chameleon.spit.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.chameleon.spit.model.Produto;

@Repository
public interface ProdutoDao extends JpaRepository<Produto, Long>, CrudRepository<Produto, Long> {

}
