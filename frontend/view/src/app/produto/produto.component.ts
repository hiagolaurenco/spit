import { BreakpointObserver } from '@angular/cdk/layout';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import Swal from 'sweetalert2';
import { ProdutoService } from '../service/produto.service';

@Component({
  selector: 'app-produto',
  templateUrl: './produto.component.html',
  styleUrls: ['./produto.component.css']
})
export class ProdutoComponent implements OnInit {

  tabela: MatTableDataSource<any[]> = new MatTableDataSource<any[]>([]);
  filteredData: MatTableDataSource<any> = new MatTableDataSource<any[]>([]);
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  displayedColumns = ['descricao','enumTipounidade', 'precoCusto', 'precoBase', 'acao'];

  constructor(
    private breakpointObserver: BreakpointObserver,
    private produtoService: ProdutoService,
    private dialog: MatDialog
  ) {
    this.breakpointObserver.observe(['(max-width: 600px)']).subscribe(result => {
      this.displayedColumns = result.matches ?
      ['descricao','enumTipounidade', 'precoCusto', 'precoBase', 'acao']:
      ['descricao','enumTipounidade', 'precoCusto', 'precoBase', 'acao'];

    });
   }

  ngOnInit(): void {
    this.listAll();
    this.filteredData.paginator = this.paginator;
    this.filteredData.sort = this.sort;
    this.tabela.paginator = this.paginator;
    this.tabela.sort = this.sort;
  }

  listAll() {
    this.produtoService.listAll().subscribe(response => {
      console.log(response);
      this.tabela.data = response;
      this.filteredData.data = response;
    })
  }

  edit(obj){
    console.log(obj);
    this.produtoService.findById(obj.id).subscribe(resp => {
      console.log(resp);
      const dialogRef = this.dialog.open(DialogProduto, {
        height: 'auto',
        width: '1200px'
      });
      dialogRef.componentInstance.produto = resp;
    })
  }

  delete(obj){
    console.log(obj);
    Swal.fire({
      title: 'Você tem Certeza?',
      text: "Você tem certeza que deseja excluir esse registro ?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sim, deletar!'
    }).then((result) => {
      if (result.isConfirmed) {
        console.log("aquiiii!!")
        this.produtoService.delete(obj.id).subscribe(resp => {
          console.log(resp);
          Swal.fire(
            'Deletado!',
            'Você excluiu esse registro com sucesso!',
            'success'
          )
          this.listAll();
        }, error=> {
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Desculpe, algo deu errado!',
          })
          this.listAll();
        });
      }
    })
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.filteredData.filter = filterValue.trim().toLowerCase();
  }

  openDialog() {
    console.log();
    const dialogRef = this.dialog.open(DialogProduto, {
      height: 'auto',
      width: '1200px'
    });
  }

}

@Component({
  selector: 'app-dialog-produto',
  templateUrl: './dialog-produto.html',
})
export class DialogProduto implements OnInit {
  produto = {
    descricao: '',
    enumTipounidade: '',
    precoCusto: null,
    precoBase: null
  }

  constructor(
    private produtoService: ProdutoService,
  ) { }

  ngOnInit() {
  }

  save() {
    console.log(this.produto)
    if (this.produto.descricao != '') {
      this.produtoService.save(this.produto).subscribe(() => {
        Swal.fire({
          icon: 'success',
          title: 'Produto Salvo com Sucesso !',
          showConfirmButton: false,
          timer: 1500
        })
      }, () => {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Desculpe, algo deu errado!',
        })
      });
    } else {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Os campos, "Produto" ou "Nome" não podem ser vazios!',
      })
    }

  }
}
