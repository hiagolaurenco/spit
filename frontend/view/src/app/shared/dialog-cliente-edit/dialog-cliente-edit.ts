import { Component, OnInit } from '@angular/core';
import { ClienteService } from 'src/app/service/cliente.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-dialog-cliente-edit',
  templateUrl: './dialog-cliente-edit.html',
})
export class DialogClienteEdit implements OnInit {
  cliente =  {
    pessoa: {
      nome: null,
      cpfCnpj: null,
      tipoPessoa: ''
    },
    endereco: null,
    email: null,
    whatsApp: null,
    statusPagamento: null,
    createdAt: null,
    updatedAt: null
  }

  maskCnpj = [/[0-9]/, /[0-9]/, '.', /[0-9]/, /[0-9]/, /[0-9]/, '.', /[0-9]/, /[0-9]/, /[0-9]/, '/', /[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/, '-', /[0-9]/, /[0-9]/]

  constructor(private clienteService: ClienteService) { }

  ngOnInit() {
  }

  update() {
    if (this.cliente.pessoa.nome != '') {
      this.clienteService.save(this.cliente).subscribe(resp => {
        Swal.fire({
          icon: 'success',
          title: 'Atualizado com Sucesso !',
          showConfirmButton: false,
          timer: 1500
        })
      }, error => {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Desculpe, algo deu errado!',
        })
      });
    } else {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Os campos, "Cliente" ou "Nome" não podem ser vazios!',
      })
    }

  }
}