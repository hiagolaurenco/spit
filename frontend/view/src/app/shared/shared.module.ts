import { NgModule } from '@angular/core';

import { MenuItems } from './menu-items/menu-items';
import { AccordionAnchorDirective, AccordionLinkDirective, AccordionDirective } from './accordion';
import { CommonModule } from '@angular/common';
import { DemoMaterialModule } from '../demo-material-module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatInputModule } from '@angular/material/input';
import { TextMaskModule } from 'angular2-text-mask';
import { CpfCnpjPipe } from './pipes/cpfcnpj.pipe';
import { DialogCliente } from '../cliente/cliente.component';
import { DialogClienteEdit } from './dialog-cliente-edit/dialog-cliente-edit';


@NgModule({
  declarations: [
    AccordionAnchorDirective,
    AccordionLinkDirective,
    AccordionDirective,
    CpfCnpjPipe,
    DialogClienteEdit
  ],
  imports:[
    CommonModule,
    DemoMaterialModule,
    FormsModule,
    FlexLayoutModule,
    MatInputModule,
    TextMaskModule,
    ReactiveFormsModule
  ],
  exports: [
    AccordionAnchorDirective,
    AccordionLinkDirective,
    AccordionDirective,
    CpfCnpjPipe,
   ],
   entryComponents: [DialogClienteEdit],
  providers: [ MenuItems ]
})
export class SharedModule { }
