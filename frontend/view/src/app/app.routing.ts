import { Routes } from '@angular/router';
import { AssociacaoComponent } from './associacao/associacao.component';
import { ClienteComponent } from './cliente/cliente.component';

import { FullComponent } from './layouts/full/full.component';
import { PedidoComponent } from './pedido/pedido.component';
import { ProdutoComponent } from './produto/produto.component';

export const AppRoutes: Routes = [
  {
    path: '',
    component: FullComponent,
    children: [
      {
        path: '',
        redirectTo: '/dashboard',
        pathMatch: 'full'
      },
      {
        path: 'cliente',
        component: ClienteComponent
      },
      {
        path: 'produto',
        component: ProdutoComponent
      },
      {
        path: 'pedido',
        component: PedidoComponent
      },
      {
        path: 'associacao',
        component: AssociacaoComponent
      },
      {
        path: '',
        loadChildren:
          () => import('./material-component/material.module').then(m => m.MaterialComponentsModule)
      },
      {
        path: 'dashboard',
        loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule)
      }
    ]
  }
];
