import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";
import { Observable } from "rxjs";
import { environment } from '../../environments/environment';

const SERVICE = environment.apiUrl;

@Injectable({
    providedIn: 'root'
})
export class ProdutoService {

    constructor(private http: HttpClient, private router: Router){}

    listAll(): Observable<any> {
        return this.http.get(`${SERVICE}/produto/listAll`);
    }

    save(obj: any): Observable<any> {
        return this.http.post(`${SERVICE}/produto/save`, obj);
    }

    delete(id: any): Observable<any> {
        return this.http.get(`${SERVICE}/produto/delete/${id}`);
    }

    findById(id: any): Observable<any> {
        return this.http.get(`${SERVICE}/produto/find-by-id/${id}`);
    }

}
