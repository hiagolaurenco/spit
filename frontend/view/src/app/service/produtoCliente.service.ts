import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";
import { Observable } from "rxjs";
import { environment } from '../../environments/environment';

const SERVICE = environment.apiUrl;

@Injectable({
    providedIn: 'root'
})
export class ProdutoClienteService {

    constructor(private http: HttpClient, private router: Router) { }

    save(obj: any): Observable<any> {
        return this.http.post(`${SERVICE}/produtoCliente/save`, obj);
    }

    listAll(): Observable<any> {
        return this.http.get(`${SERVICE}/produtoCliente/listAll`);
    }

    delete(id: any): Observable<any> {
        return this.http.get(`${SERVICE}/produto/delete/${id}`);
    }

    findById(id: any): Observable<any> {
        return this.http.get(`${SERVICE}/produto/find-by-id/${id}`);
    }

    listProduto(clienteId: any): Observable<any> {
        console.log(clienteId);
        return this.http.get(`${SERVICE}/produtoCliente/listProdutoCliente/${clienteId}`);
    }

}
