import { BreakpointObserver } from '@angular/cdk/layout';
import { ViewChild } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import Swal from 'sweetalert2';
import { ClienteService } from '../service/cliente.service';
import { DialogClienteEdit } from '../shared/dialog-cliente-edit/dialog-cliente-edit';

export interface Pessoa{
  nome: string
  cpfCnpj: string
  tipoPessoa: string
}

@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.css']
})
export class ClienteComponent implements OnInit {

  maskCnpj = [/[0-9]/, /[0-9]/, '.', /[0-9]/, /[0-9]/, /[0-9]/, '.', /[0-9]/, /[0-9]/, /[0-9]/, '/', /[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/, '-', /[0-9]/, /[0-9]/]

  tabela: MatTableDataSource<any[]> = new MatTableDataSource<any[]>([]);
  filteredData: MatTableDataSource<any> = new MatTableDataSource<any[]>([]);
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  displayedColumns = ['nome','tipoPessoa', 'cpfCnpj', 'endereco', 'email', 'whatsApp','acao'];

  constructor(
    private breakpointObserver: BreakpointObserver,
    private clienteService: ClienteService,
    private dialog: MatDialog
  ) {

    this.breakpointObserver.observe(['(max-width: 600px)']).subscribe(result => {
      this.displayedColumns = result.matches ?
      ['nome','tipoPessoa', 'cpfCnpj', 'endereco', 'email', 'whatsApp','acao'] :
      ['nome','tipoPessoa', 'cpfCnpj', 'endereco', 'email', 'whatsApp','acao'];

    });
  }

  ngOnInit(): void {
    this.listAll();
    this.filteredData.paginator = this.paginator;
    this.filteredData.sort = this.sort;
    this.tabela.paginator = this.paginator;
    this.tabela.sort = this.sort;
  }

  listAll() {
    this.clienteService.listAll().subscribe(response => {
      console.log(response);
      this.tabela.data = response;
      this.filteredData.data = response;
    })
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.filteredData.filter = filterValue.trim().toLowerCase();
  }

  edit(obj){
    console.log(obj);
    this.clienteService.findById(obj.id).subscribe(resp => {
      console.log(resp);
      const dialogRef = this.dialog.open(DialogClienteEdit, {
        height: 'auto',
        width: '1200px'
      });
      dialogRef.componentInstance.cliente = resp;
    })
  }

  delete(obj){
    console.log(obj);
    Swal.fire({
      title: 'Você tem Certeza?',
      text: "Você tem certeza que deseja excluir esse registro ?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sim, deletar!'
    }).then((result) => {
      if (result.isConfirmed) {
        console.log("aquiiii!!")
        this.clienteService.delete(obj.id).subscribe(resp => {
          console.log(resp);
          Swal.fire(
            'Deletado!',
            'Você excluiu esse registro com sucesso!',
            'success'
          )
          this.listAll();
        }, error=> {
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Desculpe, algo deu errado!',
          })
          this.listAll();
        });
      }
    })
  }

  openDialog() {
    console.log();
    const dialogRef = this.dialog.open(DialogCliente, {
      height: 'auto',
      width: '1200px'
    });
  }

}

@Component({
  selector: 'app-dialog-cliente',
  templateUrl: './dialog-cliente.html',
})
export class DialogCliente implements OnInit {

  cliente =  {
    pessoa: {
      nome: null,
      cpfCnpj: null,
      tipoPessoa: 'PF'
    },
    endereco: null,
    email: null,
    whatsApp: null,
    statusPagamento: null,
    createdAt: null,
    updatedAt: null
  }

  maskCnpj = [/[0-9]/, /[0-9]/, '.', /[0-9]/, /[0-9]/, /[0-9]/, '.', /[0-9]/, /[0-9]/, /[0-9]/, '/', /[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/, '-', /[0-9]/, /[0-9]/]

  constructor(
    private clienteService: ClienteService,
    private dialog: MatDialog
  ) { }

  ngOnInit() {
  }

  telCelMask = function (rawValue) {
    const nb = rawValue.match(/\d/g);
    let numberLength = 0;
    if (nb) {
      numberLength = nb.join('').length;
    }
    if (numberLength <= 10) {
      return ['(', /[0-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
    } else {
      return ['(', /[0-9]/, /\d/, ')', ' ', /\d/, ' ', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
    }
  }

  cpfcnpjmask = function (rawValue) {
    const nb = rawValue.match(/\d/g);
    let numberLength = 0;
    if (nb) { numberLength = nb.join('').length; }
    if (numberLength <= 11) {
      return [/[0-9]/, /[0-9]/, /[0-9]/, '.', /[0-9]/, /[0-9]/, /[0-9]/, '.', /[0-9]/, /[0-9]/, /[0-9]/, '-', /[0-9]/, /[0-9]/];
    } else {
      return [/[0-9]/, /[0-9]/, '.', /[0-9]/, /[0-9]/, /[0-9]/, '.',
        /[0-9]/, /[0-9]/, /[0-9]/, '/', /[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/, '-', /[0-9]/, /[0-9]/];
    }
  }

  save() {
    console.log(this.cliente);
    if (this.cliente.pessoa.nome != '') {
      this.clienteService.save(this.cliente).subscribe(() => {
        Swal.fire({
          icon: 'success',
          title: 'Cliente Salvo com Sucesso !',
          showConfirmButton: false,
          timer: 1500
        })
      }, () => {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Desculpe, algo deu errado!',
        })
      });
    } else {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Os campos, "Cliente" ou "Nome" não podem ser vazios!',
      })
    }

  }
}

