import { BreakpointObserver } from '@angular/cdk/layout';
import { Component, OnInit } from '@angular/core';
import { ClienteService } from '../service/cliente.service';
import { ProdutoClienteService } from '../service/produtoCliente.service';

@Component({
  selector: 'app-pedido',
  templateUrl: './pedido.component.html',
  styleUrls: ['./pedido.component.css']
})
export class PedidoComponent implements OnInit {

  clientes: any[] = [];
  clientesFiltered: any[] = [];
  produtoCliente: any[] = [];
  txtCliente = '';
  listProdutoCliente = [];
  count = 0;
  cliente =  {
    pessoa: {
      nome: null,
      cpfCnpj: null,
      tipoPessoa: ''
    },
    endereco: null,
    email: null,
    whatsApp: null,
    statusPagamento: null,
    createdAt: null,
    updatedAt: null
  }

  displayedColumns = ['descricao','enumTipounidade', 'precoCusto', 'precoBase', 'botao', 'acao'];

  constructor(
    private breakpointObserver: BreakpointObserver,
    private clienteService: ClienteService,
    private produtoClienteService: ProdutoClienteService
  ) {
    this.breakpointObserver.observe(['(max-width: 600px)']).subscribe(result => {
      this.displayedColumns = result.matches ?
      ['descricao','enumTipounidade', 'precoCusto', 'precoBase', 'precoVenda', 'botao', 'acao'] :
      ['descricao','enumTipounidade', 'precoCusto', 'precoBase', 'precoVenda', 'botao', 'acao'];

    });
  }

  ngOnInit(): void {
    this.getCliente();
  }

  getCliente(): void {
    this.clienteService.listAll().subscribe(r => {
      this.clientes = r;
      this.clientesFiltered = r;
    });
  }

  setCliente(cliente): void {
    console.log(cliente)
    this.cliente.pessoa.nome = cliente.pessoa.nome;
    this.cliente.pessoa.cpfCnpj = cliente.pessoa.cpfCnpj;
    this.cliente.pessoa.tipoPessoa = cliente.pessoa.tipoPessoa;
    this.cliente.endereco = cliente.endereco;
    this.cliente.email = cliente.email;
    this.cliente.whatsApp = cliente.whatsApp;
    this.cliente.statusPagamento = cliente.statusPagamento;
    this.cliente.createdAt = cliente.createdAt;
    this.cliente.updatedAt = cliente.updatedAt;
    this.txtCliente = cliente.pessoa.nome;

    this.produtoClienteService.listProduto(cliente.id).subscribe(r => {
      this.listProdutoCliente = r;
      console.log(this.listProdutoCliente);
    });
  }
  add(){
    this.count++;
  }

  sub(){
    this.count--;
  }

  filterCliente(clienteDesc) {
    if (clienteDesc.value) {
      this.clientesFiltered = this.clientes.filter(i => i.pessoa.nome.toLowerCase().includes(clienteDesc.value.toLowerCase()) && i.pessoa.nome.toLowerCase() != this.txtCliente.toLowerCase());
    } else {
      this.clientesFiltered = this.clientes.filter(i => i.pessoa.nome.toLowerCase() != this.txtCliente.toLowerCase());
      this.txtCliente = '';
    }
  }

  telCelMask = function (rawValue) {
    const nb = rawValue.match(/\d/g);
    let numberLength = 0;
    if (nb) {
      numberLength = nb.join('').length;
    }
    if (numberLength <= 10) {
      return ['(', /[0-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
    } else {
      return ['(', /[0-9]/, /\d/, ')', ' ', /\d/, ' ', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
    }
  }

  cpfcnpjmask = function (rawValue) {
    const nb = rawValue.match(/\d/g);
    let numberLength = 0;
    if (nb) { numberLength = nb.join('').length; }
    if (numberLength <= 11) {
      return [/[0-9]/, /[0-9]/, /[0-9]/, '.', /[0-9]/, /[0-9]/, /[0-9]/, '.', /[0-9]/, /[0-9]/, /[0-9]/, '-', /[0-9]/, /[0-9]/];
    } else {
      return [/[0-9]/, /[0-9]/, '.', /[0-9]/, /[0-9]/, /[0-9]/, '.',
        /[0-9]/, /[0-9]/, /[0-9]/, '/', /[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/, '-', /[0-9]/, /[0-9]/];
    }
  }

}
