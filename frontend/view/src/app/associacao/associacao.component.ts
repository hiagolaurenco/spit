import { BreakpointObserver } from '@angular/cdk/layout';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ClienteService } from '../service/cliente.service';
import { ProdutoService } from '../service/produto.service';
import { ProdutoClienteService } from '../service/produtoCliente.service';

@Component({
  selector: 'app-associacao',
  templateUrl: './associacao.component.html',
  styleUrls: ['./associacao.component.css']
})
export class AssociacaoComponent implements OnInit {

  ProdutoCliente = {
    cliente: null,
    produto: null,
    precoVenda: null
  }

  produtos: any[] = [];
  produtosFiltered: string[] = [];
  txtProduto = '';

  clientes : any[] = [];
  clientesFiltered: any[] = [];
  txtCliente = '';

  tabela: MatTableDataSource<any[]> = new MatTableDataSource<any[]>([]);
  filteredData: MatTableDataSource<any> = new MatTableDataSource<any[]>([]);
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  displayedColumns = ['produto', 'enumTipounidade', 'cliente', 'preco', 'acao'];

  constructor(
    private breakpointObserver: BreakpointObserver,
    private produtoService: ProdutoService,
    private clienteService: ClienteService,
    private produtoClienteService: ProdutoClienteService
  ) {
    this.breakpointObserver.observe(['(max-width: 600px)']).subscribe(result => {
      this.displayedColumns = result.matches ?
      ['produto', 'enumTipounidade', 'cliente', 'preco', 'acao'] :
      ['produto', 'enumTipounidade', 'cliente', 'preco', 'acao'];

    });
  }

  ngOnInit(): void {
    this.listAll();
    this.getProduto();
    this.getCliente();
    this.filteredData.paginator = this.paginator;
    this.filteredData.sort = this.sort;
    this.tabela.paginator = this.paginator;
    this.tabela.sort = this.sort;
  }

  listAll(){
    this.produtoClienteService.listAll().subscribe(r => {
        console.log(r);
        this.tabela.data = r;
        this.filteredData = r;
    });
  }

  cadastrar(){
      console.log(this.ProdutoCliente);
      this.produtoClienteService.save(this.ProdutoCliente).subscribe(r => {
        console.log(r);
        this.ProdutoCliente = {
            cliente: null,
            produto: null,
            precoVenda: null
          }
        this.listAll();
      });
  }

  getProduto(): void{
    this.produtoService.listAll().subscribe(r => {
      this.produtos = r;
      this.produtosFiltered = r;
    });
  }

  getCliente(): void{
    this.clienteService.listAll().subscribe(r => {
      this.clientes = r;
      this.clientesFiltered = r;
    });
  }

  setProduto(produto): void {
    this.ProdutoCliente.produto = produto;
    this.txtProduto = produto.descricao;
  }

  setCliente(cliente): void {
    this.ProdutoCliente.cliente = cliente;
    this.txtCliente = cliente.pessoa.nome;
  }

  filterProduto(produtoDesc) {
    if (produtoDesc.value) {
      this.produtosFiltered = this.produtos.filter(i => i.descricao.toLowerCase().includes(produtoDesc.value.toLowerCase()) && i.descricao.toLowerCase() != this.txtProduto.toLowerCase());
    } else {
      this.produtosFiltered = this.produtos.filter(i => i.descricao.toLowerCase() != this.txtProduto.toLowerCase());
      this.txtProduto = '';
      this.ProdutoCliente.produto = null;
    }
  }

  filterCliente(clienteDesc) {
    if (clienteDesc.value) {
      this.clientesFiltered = this.clientes.filter(i => i.pessoa.nome.toLowerCase().includes(clienteDesc.value.toLowerCase()) && i.pessoa.nome.toLowerCase() != this.txtCliente.toLowerCase());
    } else {
      this.clientesFiltered = this.clientes.filter(i => i.pessoa.nome.toLowerCase() != this.txtCliente.toLowerCase());
      this.txtCliente = '';
      this.ProdutoCliente.cliente = null;
    }
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.filteredData.filter = filterValue.trim().toLowerCase();
  }


}
